package com.arturogr.mytestmp.domain.repository

import androidx.paging.PagingData
import com.arturogr.mytestmp.domain.model.ResultMovie
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    suspend fun getMovies(): Flow<PagingData<ResultMovie>>
}
