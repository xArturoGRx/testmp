package com.arturogr.mytestmp.presentation.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.arturogr.mytestmp.commons.StateView
import com.arturogr.mytestmp.domain.model.ResultMovie
import com.arturogr.mytestmp.network.MoviesApi
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class HomeViewModel @Inject constructor() : ViewModel() {

    private val _stateView = MutableLiveData<String>()
    val stateView: LiveData<String> = _stateView

    private val _onLogout = MutableLiveData<Boolean>()
    val onLogout: LiveData<Boolean> = _onLogout

    private val _movies = MutableLiveData<List<ResultMovie>>()
    val movies: LiveData<List<ResultMovie>> = _movies

    fun onChangeLogout(value: Boolean) {
        _onLogout.value = value
    }

    init {
        search(1)
    }

    private fun search(page: Int) {
        setStateView(stateView = StateView.Loading.stateView)
        viewModelScope.launch {
            val response = MoviesApi.retrofitService.getData(page = page)
            if (response.isSuccessful && response.body() != null) {
                setStateView(stateView = StateView.Success.stateView)
                val dataResult = response.body()?.results
                dataResult?.let {
                    _movies.value = it
                }
            }
        }
    }

    fun setStateView(stateView: String) {
        _stateView.value = stateView
    }
}