package com.arturogr.mytestmp.presentation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.arturogr.mytestmp.commons.Constants
import com.arturogr.mytestmp.domain.model.DetailMovie
import com.arturogr.mytestmp.presentation.Routes.Detail
import com.arturogr.mytestmp.presentation.Routes.Home
import com.arturogr.mytestmp.presentation.Routes.Login
import com.arturogr.mytestmp.presentation.ui.detail.DetailScreen
import com.arturogr.mytestmp.presentation.ui.home.HomeScreen
import com.arturogr.mytestmp.presentation.ui.login.LoginScreen
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

@Composable
fun RootNavGraph(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Routes.Login.route
    ) {
        composable(Routes.Login.route) {
            LoginScreen(
                onNext = {
                    navController.navigate(route = Home.route) {
                        popUpTo(Login.route) {
                            inclusive = true
                        }
                    }
                }
            )
        }

        composable(Routes.Home.route) {
            HomeScreen(
                onNext = {
                    navController.currentBackStackEntry?.savedStateHandle?.set(
                        Constants.KEY_BUNDLE,
                        it
                    )
                    navController.navigate(route = Detail.route)
                },
                onLogout = {
                    Firebase.auth.signOut()
                    navController.navigate(route = Login.route) {
                        popUpTo(Home.route) {
                            inclusive = true
                        }
                    }
                }
            )
        }

        composable(Routes.Detail.route) {
            val result = navController.previousBackStackEntry?.savedStateHandle?.get<DetailMovie>(Constants.KEY_BUNDLE)
            DetailScreen(movie = result, onBack = {
                navController.popBackStack()
            })
        }
    }
}
