package com.arturogr.mytestmp.presentation.ui.home

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import com.arturogr.mytestmp.R
import com.arturogr.mytestmp.R.drawable
import com.arturogr.mytestmp.R.string
import com.arturogr.mytestmp.commons.StateView
import com.arturogr.mytestmp.commons.StateView.Init
import com.arturogr.mytestmp.domain.model.DetailMovie
import com.arturogr.mytestmp.domain.model.ResultMovie
import com.arturogr.mytestmp.presentation.ui.components.MyAlertDialog
import com.arturogr.mytestmp.presentation.ui.components.MyItemMovie
import com.arturogr.mytestmp.presentation.ui.components.MyProgressUI
import com.arturogr.mytestmp.presentation.ui.components.ToolBar

@Composable
fun HomeScreen(
    viewModel: HomeViewModel = hiltViewModel(),
    onNext: (DetailMovie) -> Unit = {},
    onLogout: () -> Unit = {}
) {
    val logout: Boolean by viewModel.onLogout.observeAsState(false)
    val stateView: String by viewModel.stateView.observeAsState(StateView.Init.stateView)
    val movies: List<ResultMovie> by viewModel.movies.observeAsState(listOf())

    Column {
        ToolBar(title = string.app_name, onLogout = {
            viewModel.onChangeLogout(true)
        }, onBack = {}, isLogout = true)
        MyAlertDialog(logout, stringResource(id = string.logout_title),
            icon = painterResource(id = drawable.warning),
            isCancel = true, onCancel = {
                viewModel.onChangeLogout(false)
            }, onConfirm = {
                onLogout.invoke()
            })

        when (stateView) {
            StateView.Loading.stateView -> {
                MyProgressUI()
            }

            StateView.Success.stateView -> {
                LazyColumn(modifier = Modifier.fillMaxSize()) {
                    items(
                        items = movies
                    ) { movie ->
                        MyItemMovie(movie, onClick = onNext)
                    }
                }
            }

            StateView.Error.stateView -> {
                MyAlertDialog(true, "error",
                    icon = painterResource(id = drawable.error), onConfirm = {
                        viewModel.setStateView(Init.stateView)
                    })
            }


        }
    }
}
