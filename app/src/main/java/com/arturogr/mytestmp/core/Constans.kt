package com.arturogr.mytestmp.commons

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

object Constants {
    const val URL_IMAGE = "https://image.tmdb.org/t/p/w780/"
    const val URL_BASE = "https://api.themoviedb.org/3/"
    const val URL_MOVIE = "movie/now_playing"
    const val P_API_KEY = "api_key"
    const val V_API_KEY = "c0823934438075d63f1dbda4023e76fc"
    const val KEY_BUNDLE = "movie"

    val MARGIN_NORMAL = 20.dp
    val MARGIN_MEDIUM = 40.dp
    val MARGIN_LARGE = 80.dp
    val TEXT_SIZE_NORMAL = 16.sp
    val TEXT_SIZE_MEDIUM = 22.sp
    val TEXT_SIZE_LARGE = 32.sp

}