package com.arturogr.mytestmp.network

import com.arturogr.mytestmp.commons.Constants
import com.arturogr.mytestmp.domain.model.MovieResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private val retrofit = Retrofit.Builder()
    .addConverterFactory(GsonConverterFactory.create())
    .baseUrl(Constants.URL_BASE)
    .build()

interface MoviesService {

    @GET(Constants.URL_MOVIE)
    suspend fun getData(
        @Query(Constants.P_API_KEY) apiKey: String = Constants.V_API_KEY,
        @Query("page") page: Int): Response<MovieResponse>
}

object MoviesApi {
    val retrofitService : MoviesService by lazy {
        retrofit.create(MoviesService::class.java)
    }
}